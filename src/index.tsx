import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

//描述方格的值以及游戏的当前状态(根据点击的方格情况判断游戏是否结束)
function Square(props:{
	value:number,
	onClick:any,
}){
	return(
		<button className="square" onClick={props.onClick}>
		{props.value}
		</button>
	);
}


// 描述游戏的整个棋盘，其中每个方格的数据和状态从square获取
class Board extends React.Component<typeof Square>{
	renderSquare(i:number){
		return <Square value={this.props.squares[i]}
		onclick={() => this.props.onclick(i)}
		/>
	}

	render(){		
		return(
			<div>
				<div className="row">
					{this.renderSquare(0)}
					{this.renderSquare(1)}
					{this.renderSquare(2)}
				</div>
				<div className="row">
					{this.renderSquare(3)}
					{this.renderSquare(4)}
					{this.renderSquare(5)}
				</div>
				<div className="row">
					{this.renderSquare(6)}
					{this.renderSquare(7)}
					{this.renderSquare(8)}
				</div>
			</div>
		);
	}
}


interface gameProps{
	history:Square[],
	step:number,
	xnext:boolean,
}

//需要在Game中展示历史的操作步骤
//有自己的私有数据和生命周期
class Game extends React.Component<gameProps> {
	constructor(props){
		super(props);
		this.state={
			history:[{
				squares: Array(9).fill(null),
			}],
			xNext:true,
		}
	}

	//处理点击的操作，记录每次的方格中的棋子格式并展示,同时反转下一次的棋子格式
	//将每一步的记录都塞入history中
	handleClick(i:number){
		const history = this.state.history.slice(0, this.state.stepNumber + 1);
	    const current = history[history.length - 1];
		const squares = this.state.squares.slice();
		if(getWinner(squares) || squares[i]){
			return ;
		}
		squares[i] = this.state.Next ? 'X' : 'O';
		this.setState({
			history:history.concat([{squares:squares}]),
			xNext:!this.state.xNext});
	}


    render() {
	  	const history = this.state.history;
	  	const current = history[history.length-1];
	  	const winner = getWinner(current.squares);

	  	let status;
		if (winner) {
	      status = 'Winner: ' + winner;
		} else {
	      status = 'Next player: ' + (this.state.xNext ? 'X' : 'O');
	    }
	  	


    return (
      <div className="game">
        <div className="game-board">
          <Board  squares={current.squares} onclick={(i) => this.handleClick(i)}/>
        </div>
        <div className="game-info">
          <div>{ status }</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}


ReactDOM.render(
  <Game />,
  document.getElementById('root')
);


function getWinner(props:{squares:[]})
{
	const lines = [
	[0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]];

	for (let i = 0; i < lines.length; i++) {
	    const [a, b, c] = lines[i];
	    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
	      return squares[a];
	    }
  	}
  	return null;
}